#include "typedefs.h"
#include "XYCoordinateList.h"
#include "ObstacleList.h"
#include "JPS.h"
#include <cmath>

class Pathfinding {
private:
    XYCoordinateList *path;
    cm vehicleRadius;
public:
    ObstacleList *obstacles;

public:
    cm getVectorLength(XYCoordinate startCoord, XYCoordinate endCoord);
    degree getVectorAngle(XYCoordinate startCoord, XYCoordinate endCoord);
    Pathfinding(ObstacleList *obstacles, XYCoordinateList *path);
    Pathfinding(ObstacleList *obstacles, XYCoordinateList *path, cm vehicleRadius);
    bool calcPath(XYCoordinate startPos, XYCoordinate endPos);
    XYCoordinateList* getPath();
    bool checkPath(XYCoordinate currentPos, cm predictDistance);
    void displayPath();
    void deletePath();
};
