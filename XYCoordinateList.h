#include <unwind-cxx.h>
#include <system_configuration.h>
#include <StandardCplusplus.h>
#include <utility.h>

#include "typedefs.h"

#include <vector>


class XYCoordinateList {

private:
	std::vector<XYCoordinate> nodeList;
	std::vector<XYCoordinate>::iterator nodeListIterator;

public:
	XYCoordinateList();
	void addCoordinate(XYCoordinate newCoordinate);
	XYCoordinate* getFirstCoordinate();
	XYCoordinate* getLastCoordinate();
	XYCoordinate* getCoordinateAt(int pos);
  std::vector<XYCoordinate>* getNodeList(); // used for debugging - use if you need access to every vector-function
	int getListLength();
	void removeFirstCoordinate();
	void removeLastCoordinate();
	bool isListEmpty();
	bool containsCoordinate(XYCoordinate* searchCoordinate);
	XYCoordinate* searchCoordinate(XYCoordinate* searchCoordinate);
  std::vector<XYCoordinate>::iterator find(std::vector<XYCoordinate>::iterator first, std::vector<XYCoordinate>::iterator last, XYCoordinate* val);
};
