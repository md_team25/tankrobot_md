#include <SoftwareSerial.h>
#include "BTInterface.h"



BTInterface::BTInterface(): bluetoothSerial(SoftwareSerial(rxPin, txPin)) {
  //Variablen zum zwischenspeichern von Nachrichtenteilen
  int empfangen = 0;
  int bluetooth = 0;
  byte gelesen; //Variable für Rohdaten
  unsigned int x, y, phi;
  bool recievedOwnPostion = false; //Zur Unterscheidung der Daten (eigenes Fahrzeug oder Gegner)
  bool messageRecieved = false; //Für while Schleife in bluetoothempfangen(). Hierdurch wird die Funktion erst verlassen wenn alle Daten empfangen wurden

  // define pin modes for tx, rx:
  pinMode(rxPin, INPUT);
  pinMode(txPin, OUTPUT);
  // set the data rate for the SoftwareSerial port
  bluetoothSerial.begin(57600);
}


void BTInterface::recievePositionData() {

  //If statement for testing with fixed values
  //if (!noPosData) {
    bool recievedOwnPostion = false;
    while (!messageRecieved) { //Don't stop until everything is recieved
      if (bluetoothSerial.available() != 0) { //Was macht diese Bedingung genau? In libary nachsehen
        gelesen = bluetoothSerial.read(); //Speicher das empfangene Byte zwischen
        if ((gelesen == '@') && (bluetooth == -1)) { //Zur Erkennung des Beginn eines neuem Telegramms
          bluetooth = 0;
        }

        //Altes switch konstrukt vom letzten Semester
        //in jedem Zyklus werden neue Daten abegrufen und durch die hier vorgegbene Reihenfolge gespeichert
        //Nach 6 Byte sind die ersten 3 Integer empfangen. Die Daten werden durch die unten stehenden If-Statements dem eigenem Fahrzeug zugeordnet
        //Danach werden erneut 6 Byte in die zurückgesetzen Variablen geschrieben und dem Gegner Fahrzeug zugeordnet

        switch (bluetooth) {
          case 0: bluetooth++; //Case 0: zum Ignorieren des Startzeichens
            break;
          case 1: x = gelesen; // LS Byte wird eingelesen
            bluetooth++;
            break;
          case 2: x |= (gelesen << 8); // MS Byte wird eingelesen
            bluetooth++;
            break;
          case 3: y = gelesen; // LS Byte wird eingelesen
            bluetooth++;
            break;
          case 4: y |= (gelesen << 8); // MS Byte wird eingelesen
            bluetooth++;
            break;
          case 5: phi = gelesen; // MS Byte wird eingelesen
            bluetooth++;
            break;
          case 6: phi |= (gelesen << 8); // MS Byte wird eingelesen
            bluetooth++;
            break;
          default: bluetooth = -1; //Switch verlassen
        }
      }
      //Eigenes Fahrzeug
      if (bluetooth == 7 && recievedOwnPostion == false) {
        //Added filter:
        //Values out of bounce are not allowed
        //New values that differ more than 20cm of old values not allowed
        //            if(!(x>200)&&!(abs(meinFahrzeug.y+20) < abs(x)))meinFahrzeug.y = x;
        //            if(!(x>300)&&!(abs(meinFahrzeug.x+20) = abs(y))) meinFahrzeug.x = y;
        if (!(x > 200))meinFahrzeug.y = x;
        if (!(x > 300)) meinFahrzeug.x = y;
        if (!(phi > 360))meinFahrzeug.phi = phi;
        bluetooth = 1;
        //Rücksetzen der Zwischenspeicher für Rohdaten
        x = 0x0000;
        y = 0x0000;
        phi = 0x0000;
        recievedOwnPostion = true;
      }            //Gegner Fahrzeug
      else if (bluetooth == 7 && recievedOwnPostion == true) {
        if (!(x > 200))gegnerFahrzeug.y = x;
        if (!(x > 300))gegnerFahrzeug.x = y;
        if (!(phi > 360))gegnerFahrzeug.phi = phi;
        //Rücksetzen der Zwischenspeicher für Rohdaten
        x = 0x0000;
        y = 0x0000;
        phi = 0x0000;
        recievedOwnPostion = false; //Reset for next cycle
        bluetooth = -1; //Prevent going into switch again
        messageRecieved = true; //For usage of this function in a while loop
      }
    }
    messageRecieved = false;
  //}
}

unsigned int BTInterface::getX() {
  return meinFahrzeug.x;
}

unsigned int BTInterface::getY() {
  return meinFahrzeug.y;
}

unsigned int BTInterface::getPhi() {
  return meinFahrzeug.phi;
}
void BTInterface::setTestPos(int x, int y, int phi) {
  meinFahrzeug.x = x;
  meinFahrzeug.y = y;
  meinFahrzeug.phi = phi;
}


XYCoordinate BTInterface::getEnemyPosition() {
  XYCoordinate enemy;
  enemy.x = gegnerFahrzeug.x;
  enemy.y = gegnerFahrzeug.y;
  return enemy;
}

XYCoordinate BTInterface::receiveXY() {
  XYCoordinate me;
  me.x = meinFahrzeug.x;
  me.y = meinFahrzeug.y;
  return me;
}
