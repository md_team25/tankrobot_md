#ifndef BTINTERFACE_H
#define BTINTERFACE_H

#include "Arduino.h"
#include "typedefs.h"
#include <SoftwareSerial.h>

class BTInterface {

private:
	int empfangen;
	int bluetooth;
	byte gelesen;
	unsigned int x,y,phi;
	bool recievedOwnPosition=false;
	bool messageRecieved=false;
	struct vehiclePos{
		unsigned int x;
		unsigned int y;
		unsigned int phi;
	} meinFahrzeug, gegnerFahrzeug;
	
public:
  SoftwareSerial bluetoothSerial;
	BTInterface();
	~BTInterface();
	
	
	//Getter
	unsigned int getX();
	unsigned int getY();
	unsigned int getPhi();
	XYCoordinate getEnemyPosition();
  XYCoordinate receiveXY();
	void recievePositionData();
 void setTestPos(int x, int y, int phi);
 
};
#endif
