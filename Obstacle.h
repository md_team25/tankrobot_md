/* 
 * File:   Obstacle.h
 * Author: Florian
 *
 * Created on 9. Dezember 2015, 08:58
 */

#ifndef OBSTACLE_H
#define	OBSTACLE_H

#include <cstdlib>
#include "typedefs.h"


class Obstacle {
public:
    Obstacle();
    Obstacle(XYCoordinate _center);
    Obstacle(XYCoordinate _center, cm _radius);
    Obstacle(XYCoordinate _center, cm _radius, char _type);
    virtual ~Obstacle();
    char getType();
    void setType(char _type);
    cm getRadius();
    void setRadius(cm _radius);
    XYCoordinate getCenter();
    XYCoordinate* getPointerToCenter();
    void setCenter(XYCoordinate _center);
    friend bool operator==(const Obstacle& lhs, const Obstacle& rhs);
    friend bool operator!=(const Obstacle& lhs, const Obstacle& rhs);
private:
    XYCoordinate center;
    cm radius;
    char type;
};

#endif	/* OBSTACLE_H */
