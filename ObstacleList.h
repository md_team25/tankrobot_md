#include "typedefs.h"
#include <vector>
#include <cmath>
#include "Obstacle.h"

#define OBSTITERATOR std::vector<Obstacle>::iterator


class ObstacleList {

public: //was private...debugging
	std::vector<Obstacle> obstList;
  OBSTITERATOR find(OBSTITERATOR first, OBSTITERATOR last, XYCoordinate searchval);
  OBSTITERATOR find(OBSTITERATOR first, OBSTITERATOR last, char searchval);

public:
	ObstacleList();
	void addObstacle(Obstacle newObstacle);
	Obstacle* getObstacleAt(XYCoordinate center);
	int getListLength();
	bool isListEmpty();
	bool isObstacle(XYCoordinate check, cm vehicleRadius = 0);
	bool moveObstacle(char type, XYCoordinate newCenter);
	bool removeObstacle(char type);
	bool removeObstacle(XYCoordinate center);
	Obstacle* getFirstObstacle();
  Obstacle* getLastObstacle();
  Obstacle* getObstacleWithType(char type);
};

