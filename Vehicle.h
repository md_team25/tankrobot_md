#include "typedefs.h"
#include <cmath>
#include "Pathfinding.h"
#include <Arduino.h>
#include <math.h>
#include "BTInterface.h"

class Vehicle {

public:
//private:
	degree rotation;
	XYCoordinate position;
	XYCoordinate goalCoordinate;
	XYCoordinate enemyPos; //should be somewhere else at some point
	cm length;
	cm width;
	int speedL;
	int speedR;
	bool onTheWay;
	degree goalAngle;
	cm goalDist;
  unsigned long rotationStart;
  bool rotating;
  bool turnPlease;
  unsigned long drivingStartTime;
  unsigned long motorOnTime;
  unsigned long motorOffTime;
  
  BTInterface *bt;

  void stop();
  void rotateL(int speed);
  void rotateR(int speed);
	void calcGoalAngle(XYCoordinate *newGoalCoordinate);
	void checkMotorLimit();
	void getGoing();
	void keepDriving();
	void holdTrack();
  
	degree toRad(int angle);


public:
	Vehicle(BTInterface *bt,Pathfinding *path);
	bool rotate(degree angle);
  void rotate45to0();
  void setMotors();
  void updateMotorSpeed();
	void drive(bool dir);
	void driveTo(XYCoordinate *newGoalCoordinate);
	void updateInterfaces();
  void avoidObstacle();
        Pathfinding *pathfinder;
        XYCoordinate getPosition();
        degree getRotation();
  bool reachedCoordinate(XYCoordinate* goalCoordinate, XYCoordinate currentCoordinate);
};
