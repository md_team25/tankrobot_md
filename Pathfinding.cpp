#include "Pathfinding.h"
#include "Arduino.h"

/* Die Klasse MyGrid wird an den Pathfinder uebergeben. In dieser muss zwingend der folgende Operator definiert werden:
 * inline bool operator()(unsigned x, unsigned y) const{}
 * Dieser muss TRUE zurueckgeben, wenn diese Koordinaten erreichbar sind und FALSE, wenn diese Koordinaten nicht erreichbar sind.
 * In unserem Fall prüfen wir also erst ob die Koordinaten x & y innerhalb des Spieldfeldbereichs sind und danach nutzen wir die
 * Funktion "isObstacle(XYCoordinate, int)" der Klasse ObstacleList um herauszufinden ob sich an dieser Stelle ein Hindernis befindet
 * oder nicht. Da Jump Point Search diesen Operator bei jeder Koordinate aufruft sicherzustellen, dass die Position erreichbar ist,
 * sollte dieser möglichst schnell gestaltet werden.
 *
 * Nachdem der Operator definiert ist, kann man die Klasse an die Klasse JPS übergeben.
 */

class MyGrid {
public:
    unsigned int w, h, r;
    ObstacleList *oblist;

    ~MyGrid() {

    }

    MyGrid(ObstacleList *obl, unsigned int width, unsigned int height, unsigned int vradius) {
        w = width;
        h = height;
        oblist = obl;
        r = vradius;
    }

    inline bool operator()(unsigned x, unsigned y) const {
        if (x < w && y < h) {
            XYCoordinate temp;
            temp.x = x;
            temp.y = y;
            if (!(oblist->isObstacle(temp, r+PATHFINDING_TOLERANCE))) {
                return true;
            }
        }
        return false;
    }
};

/* Die Klasse Pathfinder benötigt eine Liste mit Hindernissen (eine ObstacleList) und eine Liste mit XYKoordinaten (XYCoordinateList).
 * Mit allen Hindernissen in der ObstacleList wird bei der Wegberechnung und bei "checkPath()" verglichen ob der Pfad frei ist.
 * In die XYCoordinateList werden alle Koordinaten gespeichert, die abgefahren werden muessen um die Zielkoordinate zu erreichen.
 * "vehicleRadius" ist der Radius des Fahrzeugs. Dieser entspricht der Entfernung mit der ein Hindernis umfahren werden muss.
 */

Pathfinding::Pathfinding(ObstacleList *obstacles, XYCoordinateList *path) {
    this->path = path;
    this->obstacles = obstacles;
    this->vehicleRadius = tankRadius;
}

Pathfinding::Pathfinding(ObstacleList *obstacles, XYCoordinateList *path, cm radius) {
    this->path = path;
    this->obstacles = obstacles;
    this->vehicleRadius = radius;
}

/* "calcPath" ist dafür da die richtigen Werte im richtigen Format an die JPS zu uebergeben. "JPS::findPath()" benoetigt zur
 * Berechnung des Weges einen JPS::PathVector in dem die Koordinaten zwischengespeichert werden, ein Spielfeld mit dem o.g.
 * Operator, die Startpostion mit x und y Koordinate und die Zielposition mit x und y Koordinate.
 * Sobald der Weg gefunden wurde, wird dieser Stueck fuer Stueck an die XYCoordinateList angehaengt.
 * Wird kein Weg gefunden, weil der Weg zum Ziel beispielsweise komplett blockiert ist, oder sich das Ziel außerhalb des Bereichs
 * befindet, dann gibt die Funktion FALSE zurueck.
 */

bool Pathfinding::calcPath(XYCoordinate startPos, XYCoordinate endPos) {

    MyGrid grid(obstacles, FIELDWIDTH, FIELDHEIGHT, vehicleRadius);

    JPS::PathVector cpath;
    bool found = JPS::findPath(cpath, grid, startPos.x, startPos.y, endPos.x, endPos.y);

    if (found) {
        for (JPS::PathVector::iterator it = cpath.begin(); it != cpath.end(); ++it) {
            XYCoordinate newWaypoint;
            newWaypoint.x = it->x;
            newWaypoint.y = it->y;
            (*path).addCoordinate(newWaypoint);
        }
    }
    return found;
}

// Gibt den Pfad zum Ziel zurueck. Da alle Attribute "private" sind, muss fuer diese ein Getter eingefuehrt werden.

XYCoordinateList* Pathfinding::getPath() {
    return this->path;
}

/* checkPath hat folgende Funtkion
 * true -> path is clear
 * false -> path is blocked
 * false -> no path
 * Umsetzung:
 * Als erstes wird geprueft ob die Liste leer ist. Ist dies der Fall, ist noch gar kein Pfad vorhanden. Wenn
 * ein Pfad vorhanden ist, dann wird die erste Koordinate der Liste aufgerufen. Diese entspricht immer der (Teilziel-)Koordinate
 * zu der aktuell gefahren wird. Zu dieser Koordinate wird der Winkel und die Entfernung von der aktuellen Position aus berechnet.
 * Anhand dieser Daten werden dann alle Punkte zwischen der aktuellen Koordinate und dem naechsten Wegpunkt mit Hilfe der "isObstacle"
 * Funktion auf Hindernisse untersucht. 
 * Mit Hilfe dieser "checkPath"-Funktion ist es moeglich auf dynamische Hindernisse zu reagieren. Wenn sich also ein Hindernis waehrend 
 * der Fahrt veraendert, dann wird in dieser Funktion der Einfluss auf des verschobenen Hindernisses auf den geplanten Weg geprueft. Die
 * "predictDistance" muss duch Testen bestimmt werden. Es sollte nicht zu weit voraus geschaut werden, da sich das Hindernis bis zum
 * eigenen Eintreffen bereits wieder aus dem Weg verschwunden ist. Wird aber dagegen zu kurz voraus geschaut, kann es passieren, dass
 * das Hindernis nicht mehr umfahren werden kann oder im Ernstfall nicht mehr rechtzeitig auf das Hindernis reagiert werden kann. 
 *
 * Da die Entfernung zwischen aktueller Position und naechstem Wegpunkt in einigen Faellen durchaus geringer sein kann als die gewuenschte 
 * "predictDistance", wurde zusaetzlich implementiert, dass auch die folgenden Wegpunkte betrachtet werden, solange bis die gewuenschte
 * Entfernung komplett ueberprueft ist.
 * 
 * Die "predictDistance" sollte nur als Anhaltswert gesehen werden. Es wird nicht auf den cm genau abgeprueft ob sich innerhalb der 
 * Distanz ein Hindernis befindet. Am besten, wie oben schon erwaehnt, mit dem Wert herumprobieren bis eine gute Mischung zwischen
 * Reaktionszeit und Sicherheit entsteht.
 * Um eine 100%ig genaue Vorhersage zu erhalten muss die Funktion umgebaut werden.
 */


bool Pathfinding::checkPath(XYCoordinate currentPos, cm predictDistance) {
    XYCoordinate startCoord = currentPos;
    int j = 0;
    if (path->isListEmpty()) {
        return false;
    }
    XYCoordinate* endCoord = path->getFirstCoordinate();

    while (j < predictDistance && startCoord.x != path->getLastCoordinate()->x && startCoord.y != path->getLastCoordinate()->y) {
        degree angle = getVectorAngle(startCoord, *endCoord);
        cm vectorLength = getVectorLength(startCoord, *endCoord);
        int i;
        for ( i= 0; i < vectorLength && i <= (predictDistance - j); i++) {
            cm newLength = i;
            int x = round(newLength * cos(angle));
            int y = round(newLength * sin(angle));
            XYCoordinate temp;
            temp.x = startCoord.x + x;
            temp.y = startCoord.y + y;
            if (obstacles->isObstacle(temp, vehicleRadius)) {
                return false;
            }
        }

        j = j + i;
        startCoord = *endCoord;
        endCoord++;
    }
    return true;
}
/**/


/* Hier wird die Laenge eines Vektors zwischen den beiden uebergebenen Koordinaten berechnet. Diese
 * funktioniert mit normaler Vektorrechnung.
 */

cm Pathfinding::getVectorLength(XYCoordinate startCoord, XYCoordinate endCoord) {
    int x = endCoord.x - startCoord.x;
    int y = endCoord.y - startCoord.y;
    int vectorLength = round(sqrt(pow(abs(x), 2) + pow(abs(y), 2)));
    return vectorLength;
}

/* Hier wird der Winkel eines Vektors zwischen den beiden uebergebenen Koordinaten berechnet. Dies
 * funktioniert mit normaler Vektorrechnung.
 */

degree Pathfinding::getVectorAngle(XYCoordinate startCoord, XYCoordinate endCoord) {
    int x = endCoord.x - startCoord.x;
    int y = endCoord.y - startCoord.y;

    float phi = atan((float) y / (float) x);
    if (x >= 0 && y >= 0) {
        phi += 0;
    } else if (x < 0) {
        phi += M_PI;
    } else if (x >= 0 && y < 0) {
        phi += 2 * M_PI;
    }
    return phi;
}

/* Ausgabe des aktuellen Pfades per Serial.print */
void Pathfinding::displayPath(){
  int p = 1;
  Serial.println();
  Serial.println(" ---Calculated Path--- ");
  for(std::vector<XYCoordinate>::iterator it = path->getNodeList()->begin(); it != path->getNodeList()->end(); ++it){
    Serial.print("Waypoint ");
    Serial.print(p);
    Serial.print(": x=");
    Serial.print(it->x);
    Serial.print(" y=");
    Serial.println(it->y);
    p++;
  }
  Serial.println();
}

/* Löschen aller Listenelemente des Pfades */
void Pathfinding::deletePath(){
  while(!path->isListEmpty()){
    if(DEBUGLVL >= 2) Serial.println("Main: removed last coordinate from path");
    path->removeLastCoordinate();
  }
}

