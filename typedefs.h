#ifndef __TYPEDEFS_H_INCLUDED__
#define __TYPEDEFS_H_INCLUDED__

//#include "Arduino.h"
//#include "Obstacle.h"

/* Debuglevel 
 * Set the Debuglevel to a number betwen 0 and 4 to get more or less detailed
 * debug prints. Debug prints that belong to a smaller number then selected
 * will be displayed too.
 * - Not perfect yet. Created during cleanups after the project was finished.
 * 
 * 0 -> no prints
 * 1 -> position, rotation, enemyPos, next goal point, goal distance
 * 2 -> information about the path
 * 3 -> every function prints what happens !!loop runtime is very long!!
 * 4 -> everything sended or received is printed
 */
#define DEBUGLVL 1
#define noPosData false

/* Field-Size */
#define FIELDWIDTH 200 //x
#define FIELDHEIGHT 300 //y

/* Vehicle-Select 
 * Some things have to be set differently on the robots (like offesets for the motors). 
 * Define which robot you are flashing here. Otherwise wrong parameters will cause weird behavior.
 */
#define BLUE //chose which vehicle you are using [RED/BLUE]

/* Motors 
 * M1 is Motor one, the left motor (if u look in driving direction)
 * M2 is Motor two, the right motor (if u look in driving direction)
 * MxDir -> selects rotation direction: LOW -> forward, HIGH -> backward 
 * MxBrk -> Motor break: LOW -> Break off (to drive), HIGH -> Break on (to stop) 
 * MxSpeed -> sets rotation speed of the motor: [0 (0V) ... 255 (Batterie Voaltage)]
 */
#define M1Dir 13 
#define M1Brk 8 
#define M1Speed 11 
#define M2Dir 12 
#define M2Brk 9 
#define M2Speed 3 
#define rxPin 10
#define txPin 11



/* Size of the robot that the path planning algorithm takes into account */
#define tankRadius 15

/* Defines how accuarate the robot tries to reach an angle when method: rotate(degree) is called */
#define rotationTolerance 5

/* Defines the distance the path planning algorithm plans between the path an obstacles.
 * - the bigger -> the safer: The vehicle drives far away from obstacles.
 *                             BUT: Path planning fails more often because it is often not possible 
 *                             to plan a path that keeps the selected distance.
 * - the smaller -> the faster: The vehicle passes obstacles very close.                         
 *                              BUT: If the position of the robot received from the VOR-System
 *                              is not accurate the robot might hit obstacles,
 *                              because it thinks it is somewhere else.                 
 */
#define PATHFINDING_TOLERANCE 30

/* Is the robot coordReachedTolerance close to its goal point it is accepted as "reached"*/
#define coordReachedTolerance 20

/* When the robot drives towards a goal it continuesly checks if he is still heading towards that point.
 * -As soon as his heading more than AngleToleranceLow degree off he tries to correct the angle by adjusting the speed
 *  of its motors differnetly. This happens in the method holdTrack(). 
 *    -> this is not working very well yet due to time issues. The logic behind the holdTrack method isn't 
 *        the brightest candle on the cake ;)
 * -As soon as his heading more than AngleToleranceHigh degree off the robot stops and reaims at the goal point by using
 *  the rotate(degree) mehtod
 */
#define AngleToleranceLow 5 
#define AngleToleranceHigh 30 


/* Differences between both robots:
 *  The Robots are pretty much the same but nothing is perfect. So what we have to do is set parameters differently according
 *  to which robot we are using. 
 *  - MotorMax is the maximum voltage that is set for the motors (255 -> battery Voltage)
 *  - rotationSpeed in % [0..100]
 *  - drivingSped in % [0..100]
 *  - MotorOffsetLeft is used to compensate the different motorspeed on each side of the vehicle (is added to the left motor)
 *  - ANGLEOFFSET is the angle the magnetometer outputs when the robot is headed in the wanted "0-degree-direction" (x-axis)
 *                !!! the polynom for the dynamic magnetometer offset has to be implemented
 */
#ifdef BLUE
  #define MotorMax 100
  // #define rotationSpeed 45 
  // #define drivingSpeed 30 
  #define ANGLEOFFSET 307
  
#endif

#ifdef RED
  #define MotorMax 100  //these motors are not made to take 11,1V
  // #define rotationSpeed 40
  // #define drivingSpeed 20
  #define ANGLEOFFSET 297  
#endif


/* Creating typedefs to make code easier to read */

typedef int cm;
typedef float degree;

typedef struct {
  cm x;
  cm y;
} XYCoordinate;


#endif // __TYPEDEFS_H_INCLUDED__
