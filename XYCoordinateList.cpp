#include "XYCoordinateList.h"

XYCoordinateList::XYCoordinateList() {
    nodeList.clear();
}

void XYCoordinateList::addCoordinate(XYCoordinate newCoordinate) {
    nodeList.push_back(newCoordinate);
}

XYCoordinate* XYCoordinateList::getFirstCoordinate() {
    return &nodeList.front();
}

XYCoordinate* XYCoordinateList::getLastCoordinate() {
    return &nodeList.back();
}

XYCoordinate* XYCoordinateList::getCoordinateAt(int pos) {
    return &nodeList.at(pos);
}

int XYCoordinateList::getListLength() {
    return nodeList.size();
}

std::vector<XYCoordinate>* XYCoordinateList::getNodeList(){
    return &nodeList;
}

void XYCoordinateList::removeFirstCoordinate() {
    if(!isListEmpty())nodeList.erase(nodeList.begin());
}

void XYCoordinateList::removeLastCoordinate() {
    if(!isListEmpty())nodeList.pop_back();
}

bool XYCoordinateList::isListEmpty() {
    return nodeList.empty();
}

bool XYCoordinateList::containsCoordinate(XYCoordinate* searchCoordinate) {
    nodeListIterator = find(nodeList.begin(), nodeList.end(), searchCoordinate);
    if(nodeListIterator!=nodeList.end()){
        return true;
    }
    else{
        return false;
    }
}

XYCoordinate* XYCoordinateList::searchCoordinate(XYCoordinate* searchCoordinate) {
    nodeListIterator = find(nodeList.begin(), nodeList.end(), searchCoordinate);
    if(nodeListIterator!=nodeList.end()){
        return &(*nodeListIterator);
    };
}

std::vector<XYCoordinate>::iterator XYCoordinateList::find(std::vector<XYCoordinate>::iterator first, std::vector<XYCoordinate>::iterator last, XYCoordinate* val)
{
  while (first!=last) {
    if (first->x ==val->x && first->y == val->y) return first;
    ++first;
  }
  return last;
}
