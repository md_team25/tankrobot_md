#include "ObstacleList.h"

ObstacleList::ObstacleList() {
    obstList.clear();
}

void ObstacleList::addObstacle(Obstacle newObstacle) {
    obstList.push_back(newObstacle);
}

Obstacle* ObstacleList::getObstacleAt(XYCoordinate center) {
    OBSTITERATOR it = find(obstList.begin(), obstList.end(), center);
    return it; //ATTENTION: If the coordinate was not found it returns the end value!
}

Obstacle* ObstacleList::getObstacleWithType(char type) {
    OBSTITERATOR it = find(obstList.begin(), obstList.end(), type);
    return it; //ATTENTION: If the coordinate was not found it returns the end value!
}

int ObstacleList::getListLength() {
    return obstList.size();
}

Obstacle* ObstacleList::getFirstObstacle() {
    return obstList.begin();
}

Obstacle* ObstacleList::getLastObstacle(){
    return obstList.end();
}

bool ObstacleList::isListEmpty() {
    return obstList.empty();
}

bool ObstacleList::isObstacle(XYCoordinate check, cm vehicleRadius) {
    OBSTITERATOR first = obstList.begin();
    OBSTITERATOR last = obstList.end();

    for (first; first != last; first++) { 
        Obstacle* _obst = first;
        int _radius = _obst->getRadius();
        if (check.x < (_obst->getCenter().x - _radius - vehicleRadius)) continue;
        if (check.x > (_obst->getCenter().x + _radius + vehicleRadius)) continue;
        if (check.y < (_obst->getCenter().y - _radius - vehicleRadius)) continue;
        if (check.y > (_obst->getCenter().y + _radius + vehicleRadius)) continue;
        if((pow((check.x-_obst->getCenter().x),2)+pow((check.y-_obst->getCenter().y), 2)) <= pow(_radius + vehicleRadius,2)){
            return true;
        }
    }
    return false;
}

bool ObstacleList::moveObstacle(char type, XYCoordinate newCenter) {
    OBSTITERATOR it = find(obstList.begin(), obstList.end(), type);
    if(it!=obstList.end()){
        it->getPointerToCenter()->x = newCenter.x;
        it->getPointerToCenter()->y = newCenter.y;
        return true;
    }
    else{
        return false;
    }
}

bool ObstacleList::removeObstacle(char type) {
    OBSTITERATOR it;
    bool returnval = false;
    while(it!=obstList.end()){
        it = find(obstList.begin(), obstList.end(), type);
        if(it!=obstList.end()){
            obstList.erase(it);
            returnval = true;
        }
    }
    
    return returnval;
}

bool ObstacleList::removeObstacle(XYCoordinate center) {
    OBSTITERATOR it = find(obstList.begin(), obstList.end(), center);
    if(it!=obstList.end()){
        obstList.erase(it);
        return true;
    }
    else{
        return false;
    }
}

OBSTITERATOR ObstacleList::find(OBSTITERATOR first, OBSTITERATOR last, XYCoordinate searchval)
{
  while (first!=last) {
    if (first->getCenter().x ==searchval.x && first->getCenter().y == searchval.y) return first;
    ++first;
  }
  return last;
}

OBSTITERATOR ObstacleList::find(OBSTITERATOR first, OBSTITERATOR last, char searchval)
{
  while (first!=last) {
    if (first->getType() ==searchval) return first;
    ++first;
  }
  return last;
}

