/* 
 * File:   Obstacle.cpp
 * Author: Florian
 * 
 * Created on 9. Dezember 2015, 08:58
 */

#include "Obstacle.h"

Obstacle::Obstacle() {
    this->center.x = 0;
    this->center.y = 0;
    this->radius = 0;
    this->type = 'z';
}

Obstacle::Obstacle(XYCoordinate _center){
    this->center.x = _center.x;
    this->center.y = _center.y;
    this->radius = 0;
    this->type = 'z';
}

Obstacle::Obstacle(XYCoordinate _center, cm _radius){
    this->center.x = _center.x;
    this->center.y = _center.y;
    this->radius = _radius;
    this->type = 'z';
}

Obstacle::Obstacle(XYCoordinate _center, cm _radius, char _type){
    this->center.x = _center.x;
    this->center.y = _center.y;
    this->radius = _radius;
    this->type = _type;
}

Obstacle::~Obstacle() {
}

XYCoordinate Obstacle::getCenter(){
    return center;
}

XYCoordinate* Obstacle::getPointerToCenter(){
    return &center;
}

void Obstacle::setCenter(XYCoordinate _center){
    this->center = _center;
}

cm Obstacle::getRadius(){
    return radius;
}

void Obstacle::setRadius(cm _radius){
    this->radius = _radius;
}
char Obstacle::getType(){
    return type;
}

void Obstacle::setType(char _type){
    this->type = _type;
}

bool operator==(const Obstacle& lhs, const Obstacle& rhs){
    return (lhs.center.x == rhs.center.x && lhs.center.y == rhs.center.y && lhs.type == rhs.type && lhs.radius == rhs.radius);
}

bool operator!=(const Obstacle& lhs, const Obstacle& rhs){
    return !(&lhs==&rhs);
}
