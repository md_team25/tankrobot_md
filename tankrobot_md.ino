/* Downloaded libraries */
#include <StandardCplusplus.h>
#include <utility.h>
#include <system_configuration.h>
#include <unwind-cxx.h>
#include <Wire.h>

/* Our libraries */
#include "Vehicle.h"
#include "typedefs.h"
#include "Obstacle.h"
#include "BTInterface.h"

/* Pins */
int led = 13;

/* Creating one pointer for every class needed. Remember: the objects are not
   instantiated at this point. These are just empty pointers.
*/
XYCoordinateList *path;
ObstacleList *obstacles;
Pathfinding *pathfinder;
Vehicle *tank;
BTInterface *bt;

/* Creating XYCoordinates that will be passed to the pathfinding-algorithm.
   The path will be planed to this coordinate. The x and y components of the
   coordinate are being set in the setup().
*/
XYCoordinate finalGoal;
XYCoordinate finalGoal2;



/* Create Obstacles at certain positions of the field with a radius and a type.
   The type is used to recognize and acces different obstacles later on. This is
   for example neccesary when an obstacle like an enemy vehicle cahnged its
   position during runtime. --> ({x,y} , radius, type)
   Just creating the obstacles here isn't enough. They have to be added to the
   obstacleList used by the pathfinder.

   __
  |__|
      [200,300]**********************[200,150]**********************[200,0]
 *    *********************************************************************
 *    *********************************************************************
 *    *********************************************************************
 *    *********************************************************************
 *    *********************************************************************
 *    ********************************************************************* __
      [100,300]**********************[100,150]**********************[100,0]|__|
 *    *********************************************************************
 *    *********************************************************************
 *    *********************************************************************
 *    *********************************************************************
 *    *********************************************************************
 *    *********************************************************************
   __ [0,300]*************************[0,150]*************************[0,0]
  |__|

*/
Obstacle obst1({500, 500}, 1, 'a'); //one obstacle so the list is not empty, not sure if really necessary

void setup() {
  Serial.begin(9600); // Serial -> Computer (Debugging)


  if (DEBUGLVL >= 1) {
    Serial.println();
    Serial.println("Main: entering setup()");
    Serial.println();
  }

  /*prepare Arduino on-board-LED*/
  pinMode(led, OUTPUT);
  digitalWrite(led, LOW);

  /* Create objects of every needed class by calling the constructors. The return values
     are pointers to the created objects. The returned pointers are stored in the pointers
     we created at the top of this file.
  */
  bt = new BTInterface();
  path = new XYCoordinateList();
  obstacles = new ObstacleList();
  pathfinder = new Pathfinding(obstacles, path, tankRadius);
  tank = new Vehicle(bt, pathfinder);

  /* Define where the vehicle drives to on the field.
     One goal for the RED tank, one goal for the BLUE one.
     Remember to select which car you are using in the
     typedefs.h file before flashing.
  */
#ifdef RED
  finalGoal.x = 100;
  finalGoal.y = 150;
  finalGoal2.x = 100;
  finalGoal2.y = 250;
#endif

#ifdef BLUE
  finalGoal.x = 100;
  finalGoal.y = 150;
  finalGoal2.x = 100;
  finalGoal2.y = 250;
#endif

  /* The obstacles we created at the top of this file are added
     to the ObstacleList the Pathfinder uses to calculate the path.
  */
  obstacles->addObstacle(obst1);

  /* Stop motors */
  tank->stop();


  /* Printing serveral infos about the vehicle */
  //tank->bt->setTestPos(5,5,0);
  if (DEBUGLVL >= 1) Serial.println("Waitning for pos data");
  tank->updateInterfaces();
  if (DEBUGLVL >= 1) Serial.println("pos data recieved");

  /* Calculate the first path. In this Path the other vehicle is not being planed with
      since we didn't add the other vehicle as an obstacle to the obstacleList yet.
  */
  if (DEBUGLVL >= 1) Serial.print("Did pathplanning work? ");
  if (DEBUGLVL >= 1) Serial.println(tank->pathfinder->calcPath(tank->getPosition(), finalGoal));

  /* Check and output the calculated path */
  if (DEBUGLVL >= 2) {
    Serial.print("Is the Pathlist empty? ");
    Serial.println(tank->pathfinder->getPath()->isListEmpty());
    tank->pathfinder->displayPath();
  }

  /* Add the other vehicle to the obstacleList. From now on checkPath() will pay attention
     to the other vehicle. The position of the other vehicle will be adjusted with every call
     of updateInterfaces().
  */
  Obstacle enemy({500, 500}, 15, 'e');
  obstacles->addObstacle(enemy);

  if (DEBUGLVL >= 1) Serial.println("Main: leaving setup(), entering loop()");
}

long int testTimer = millis();
int testStep = 1;
void loop() {
  /**********************************************************************/
  /*                      UPDATE INFORMATION                            */
  /**********************************************************************/
  tank->updateInterfaces();





  /**********************************************************************/
  /*              DRIVE WITH PATHNPLANNING TO FINALGOAL                 */
  /**********************************************************************/
  
    // Check if goal is already reached
    if (!tank->reachedCoordinate(&finalGoal, tank->getPosition())) {
    //if (DEBUGLVL >= 1) Serial.println("Goal not reached");
    // Goal is not reached - Check if path is blocked
    if (!tank->pathfinder->checkPath(tank->getPosition(), 25)) {
      // Path is blocked
      tank->stop();
      tank->pathfinder->deletePath();
      if (DEBUGLVL >= 1) Serial.println("Main: Path is blocked!");

      // Try to calculate a new path
      if (!tank->pathfinder->calcPath(tank->getPosition(), finalGoal)) {
        //Pathplaning didn't work
        if (DEBUGLVL >= 1) Serial.println("Vehicle inside of obstacle");
      } else {
        // Pathplaning worked
        if (DEBUGLVL >= 1) Serial.println("Main: Path planned");
      }
    } else {
      // Path is not blocked
      //if (DEBUGLVL >= 1) Serial.println("drive to");
      tank->driveTo(tank->pathfinder->getPath()->getFirstCoordinate());
    }
    } else {
    // Goal is reached
    tank->stop();
    }
    }
    /**/

  /**********************************************************************/
  /*                        SERIAL PRINTS                               */
  /**********************************************************************/
  /*
    if (DEBUGLVL >= 1) {
    Serial.print("Main: currentPosition: x=");
    Serial.print((tank->getPosition()).x);
    Serial.print(" y=");
    Serial.print((tank->getPosition()).y);
    Serial.print("\tcurrentRotation:");
    Serial.print(tank->getRotation());
    Serial.print("\t\tcurrentEnemyPosition: x=");
    Serial.print(tank->enemyPos.x);
    Serial.print(" y=");
    Serial.print(tank->enemyPos.y);
    Serial.print("\t\tfinalGoal: x=");
    Serial.print(finalGoal.x);
    Serial.print(" y=");
    Serial.print(finalGoal.y);
    Serial.print("\t\tnextCoordinate is x=");
    Serial.print(path->getFirstCoordinate()->x);
    Serial.print(" y=");
    Serial.print(path->getFirstCoordinate()->y);
    Serial.print(" (Distance: ");
    Serial.print(tank->goalDist);
    Serial.println(")");
    }

    if (DEBUGLVL >= 2) {
    Serial.print("Main: Check Path returns: ");
    Serial.println(tank->pathfinder->checkPath(tank->getPosition(), 40));
    }

    if (DEBUGLVL >= 3) {
    Serial.print("Main: Enemy obst? X:");
    Serial.print(obstacles->getObstacleWithType('e')->getCenter().x);
    Serial.print(" Y:");
    Serial.println(obstacles->getObstacleWithType('e')->getCenter().y);
    }
    /**/

  /**********************************************************************/
  /*                         MOTOR TEST                                 */
  /**********************************************************************/
  /*
    if(millis() % 2000 < 1000){
      tank->speedL = 150;
      tank->speedR = 150;
    }else{
      tank->speedL = -150;
      tank->speedR = -150;
    }
    tank->checkMotorLimit();
    tank->setMotors();
    /**/

  /**********************************************************************/
  /*                        DRIVE TEST                                  */
  /**********************************************************************/
  /*
    //Serial.println("testing motors");
    tank->onTheWay = true;
    tank->rotateL(1); //1 -> forward, 0 -> backward
    tank->updateMotorSpeed();
    /**/


  /**********************************************************************/
  /*                       ROTATION TEST                                */
  /**********************************************************************/
  /*
    tank->onTheWay = false;
    //tank->driveTo(&finalGoal);
    //
    //  if (tank->reachedCoordinate(&finalGoal, tank->getPosition())) {
    //      tank->stop();
    //  } else
    tank->drive(1);

    /**/


  /**********************************************************************/
  /*                      DRIVETO() TEST                                */
  /**********************************************************************/
  /*
  tank->driveTo(&finalGoal);
  if (tank->reachedCoordinate(&finalGoal, tank->getPosition())) {
    tank->stop();
    while (1) {
      Serial.print("Reached Coordinate:");
      tank->stop();
    }
  }
  /**/
  /**********************************************************************/
  /*                      DRIVETO() TEST                                */
  /**********************************************************************/
  /*
  if ((testTimer + 10000 < millis()) && testStep == 1) {
    Serial.println("Rotation completed");
    tank->bt->setTestPos(5, 5, 57);
    testStep++;
  }
  if ((testTimer + 25000 < millis()) && testStep == 2) {
    Serial.println("Reached goal coordiante");
    tank->bt->setTestPos(199, 299,57);
    testStep++;
  }

  tank->updateInterfaces();
  tank->driveTo(&finalGoal);
  if (tank->reachedCoordinate(&finalGoal, tank->getPosition())) {
    tank->stop();
    while (1) {
      Serial.print("Reached Coordinate:");
      tank->stop();
    }
  }
  /**/


  /**********************************************************************/
  /*                     PATHFINDING TEST                               */
  /**********************************************************************/
  /*
    if(!tank->pathfinder->checkPath(tank->getPosition(), 50)){
      Serial.println("Path is blocked");
      while(!tank->pathfinder->getPath()->isListEmpty()){
        tank->pathfinder->getPath()->removeLastCoordinate();
        Serial.println("path cleared");
      }
      if(!tank->pathfinder->calcPath(tank->getPosition(), finalGoal)){
        Serial.println("Pathplaning failed");
      }else{
        Serial.println("Path planned");
         tank->pathfinder->displayPath();
      }
    }
    delay(50);
    /**/
    


