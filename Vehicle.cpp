#include "Vehicle.h"


Vehicle::Vehicle(BTInterface *bt, Pathfinding *path) {
  this->bt = bt;
  this->pathfinder = path;

  this->rotation = 0;
  this->speedL = this->speedR = 0;
  this->goalCoordinate = {0, 0};
  this->rotating = false;
  this->onTheWay = false;
  this->turnPlease = false;
  position.x = 0;
  position.y = 0;
  goalDist = coordReachedTolerance + 1;
}


/**********************************************************************/
/*                       UPDATE INFORMATION                           */
/**********************************************************************/

/* updateInterfaces()

*/
void Vehicle::updateInterfaces() {
  if (DEBUGLVL >= 4)Serial.println("updateInterfaces(): recieving data from BT");
  this->bt->recievePositionData();
  if (DEBUGLVL >= 4)Serial.println("updateInterfaces(): trying to receive currentPos");
  XYCoordinate newPos = bt->receiveXY();
  if (newPos.x != 0 && newPos.y != 0) position = newPos; //dont use x=0, y=0 coordinates
  this->rotation = this->bt->getPhi();
  this->enemyPos = this->bt->getEnemyPosition();

  if (DEBUGLVL >= 4)Serial.println("updateInterfaces(): received enemyPos, trying to move enemy obstacle");
  pathfinder->obstacles->moveObstacle('e', enemyPos);

  if (DEBUGLVL >= 4)Serial.println("updateInterfaces(): moved enemy, leaving updateInterfaces()");
}


/**********************************************************************/
/*                          CALCULATIONS                              */
/**********************************************************************/

/* calcGoalAngle(GoalCoordinate)

*/
void Vehicle::calcGoalAngle(XYCoordinate *newGoalCoordinate) {
  int deltaX = newGoalCoordinate->x - position.x;
  int deltaY = newGoalCoordinate->y - position.y;

  degree newRotation = atan(deltaY / deltaX);
  if (deltaX >= 0 && deltaY >= 0){
    newRotation += 0;
  } else if (deltaX < 0) {
    newRotation += M_PI;
  } else if (deltaX >= 0 && deltaY < 0) {
    newRotation += 2 * M_PI;
  }

  if(deltaX==0)this->goalDist = abs(deltaY);
  if(deltaY==0)this->goalDist = abs(deltaX);
  if(deltaX == 0 && deltaY == 0)this->goalDist = 0;
  if(deltaX != 0 && deltaY != 0)this->goalDist = round(sqrt(pow(abs(deltaX),2) + pow(abs(deltaY),2)));

}

/* toRad(angle)
    is this really working? is it being used somewhere? check plz
*/
degree Vehicle::toRad(int angle) {
  return ((M_PI / 180) * angle);
}

/* reachedCoordinate(goalCoordinate, currentCoordinate)

*/
bool Vehicle::reachedCoordinate(XYCoordinate* goalCoordinate, XYCoordinate currentCoordinate) {
  int Xdelta = abs(goalCoordinate->x - currentCoordinate.x);
  int Ydelta = abs(goalCoordinate->y - currentCoordinate.y);
  //
  //  if(DEBUGLVL >= 2){
  //    Serial.print(" reachedCoordinate(): dX: ");
  //    Serial.print(Xdelta);
  //    Serial.print("\tdY: ");
  //    Serial.println(Ydelta);
  //  }

  if (Xdelta == 0 && Ydelta < coordReachedTolerance) return true;
  if (Ydelta == 0 && Xdelta < coordReachedTolerance) return true;
  if (Xdelta == 0 && Ydelta == 0) return true;
  if (Xdelta != 0 && Ydelta != 0 && round(sqrt(pow(Xdelta, 2) + pow(Ydelta, 2))) < coordReachedTolerance) return true;
  return false;
}


/**********************************************************************/
/*                          MOTOR CONTROL                             */
/**********************************************************************/

/* setMotors()

*/
void Vehicle::setMotors() {
  /* This method finaly sets the motors */
}
void Vehicle::updateMotorSpeed() {
}


/**********************************************************************/
/*                          DRIVING LOGIC                             */
/**********************************************************************/
bool Vehicle::rotate(degree angle) { //int angle = the absolute angleb
  degree angleDiff = angle - rotation;  //ToDo: filter angles above 360 degree with modulo
  float angleDiffRad = toRad(angleDiff);
  if (abs(angleDiff) <= (float)rotationTolerance) {
    stop();
    rotating = false;
    return true;
  } else {
    if (angleDiffRad < -M_PI) {
      rotateL(MotorMax);
    } else if (angleDiffRad <= 0) {
      rotateR(MotorMax);
    }
    if (angleDiffRad > M_PI) {
      rotateR(MotorMax);
    } else if (angleDiffRad >= 0) {
      rotateL(MotorMax);
    }
    return false;
  }
}

/* driveTo(XYCoordinate)

*/
void Vehicle::driveTo(XYCoordinate *newGoalCoordinate) {


  //check if we want to drive to new coordinate
  if ((this->goalCoordinate.x != newGoalCoordinate->x) && (this->goalCoordinate.y != newGoalCoordinate->y)) {
    if (DEBUGLVL >= 1)Serial.println("driveTo called with a new coordinate");
    this->goalCoordinate.x = newGoalCoordinate->x;
    this->goalCoordinate.y = newGoalCoordinate->y;
    onTheWay = false;
  }

  calcGoalAngle(newGoalCoordinate);

  if (goalDist < coordReachedTolerance) { //Are we there?
    pathfinder->getPath()->removeFirstCoordinate();   //reached coordinate so we will remove it and drive to the next one
    onTheWay = false;
    stop();
  } else { //no we are not
    if (!onTheWay) {
      getGoing();
    }
    if (onTheWay){
      holdTrack(); 
    }
  }
}

/* getGoing()

*/
void Vehicle::getGoing() {
  if (rotate(goalAngle)) {
    onTheWay = true;
    drive(80);
  }
}

/* keepDriving()

*/
//void Vehicle::keepDriving() {
//  holdTrack();
//}

/* holdTrack()

*/
//void Vehicle::holdTrack(){
////Gegenregeln abhängig von der Entferneung zum Ziel
//  //to have something written here, only wirtten an ordinary Controller
//  degree angleT = map(goalDist, 0, 150, 100, 30);
//  if(rotation < (goalAngle-angleT) || rotation > (goalAngle+angleT)){
//   stop();
//   onTheWay = false;
//  }
//  else{
//    drive(1);
//  }
//}

void Vehicle::holdTrack(){
  int tempRotationTolerance=rotationTolerance;
  degree angleDiff = abs(goalAngle - rotation);
  if(angleDiff>180)angleDiff-180; //Go the other way around couse it's shorter
  if(goalDist<50)tempRotationTolerance*=1.5;
  if(goalDist<40)tempRotationTolerance*=1.5;
  if(goalDist<30)tempRotationTolerance*=1.5;
  if(goalDist<20)tempRotationTolerance*=1.5;
  if(goalDist<15)tempRotationTolerance*=1.5;
  if(goalDist<10)tempRotationTolerance*=2;
  if(goalDist<5)tempRotationTolerance*=2;
  if(angleDiff <tempRotationTolerance){
   stop();
   onTheWay = false;
  }
  else{
    drive(1);
  }
}

/* rotate45to0()

*/

/**********************************************************************/
/*                          DRIVING BASICS                            */
/**********************************************************************/

/* drive(bool)
   The drive-method is written to drive foreward and backward.
   If drive is called with dir = true the vehicle will drive backwards.
   To make the vehicle drive straight foreward an offset was given to one Motor.
*/
void Vehicle::drive(bool dir) {

  if (dir) {
    Serial.print('1');
  } else {
    Serial.print('2');
  }
}

/* rotateL(speed)

*/
void Vehicle::rotateR(int speed) {
  int a = speed;
  Serial.print('3');
}

/* rotateR(speed)

*/
void Vehicle::rotateL(int speed) {
  int a = speed;
  Serial.print('4');
}

/* stop()

*/
void Vehicle::stop() {
  Serial.print('5');
}


/**********************************************************************/
/*                             GETTER                                 */
/**********************************************************************/

XYCoordinate Vehicle::getPosition() {
  return this->position;
}

degree Vehicle::getRotation() {
  return this->rotation;
}
